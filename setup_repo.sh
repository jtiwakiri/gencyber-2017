# Gencyber 2017
# Script to set up the local repository

##########
# Globals
##########
DIR=`pwd`
FLAG=$1
ARGS=$#

##########
# Functions
##########

##########
# Print usage
usage() {
  echo "Usage: ./setup.sh <flags>"
  echo "Flags:"
  echo "-h    display usage"
  echo "--edit-sources    Runs only the step to edit the sources.list file.  Use this if the edit previously failed because of lack of privileges."
}

##########
# Check arguments
check_args() {
  if [ ${ARGS} -gt 1 -o "${FLAG}" == "-h" -o "${FLAG}" != "--edit-sources" -a ${ARGS} -ne 0 ]; then
    usage
    exit
  fi
}

##########
# Check values of global variables
check_globals() {
  if [ ! -f "${DIR}/repository.tar.gz" ]; then
    echo "Error:  Please run setup.sh from the Gencyber-2017 directory."
    exit 1;
  fi
}

##########
# Uncompress .tar.gz
# $1 = Name of the file without the .tar.gz extension
uncompress() {
  file=$1
  gunzip "${file}.tar.gz"
  tar -xvf "${file}.tar"
}

##########
# Add additional line to the apt sources file
edit_sources() {
  echo "Editing sources.list file.  If this step fails, run as superuser with flag --edit-sources.  That is, \`sudo ./setup.sh --edit-sources\`"
  sources="/etc/apt/sources.list"
  echo "deb file:${DIR}/repository ./" >> ${sources}
}

##########
# Main
##########
check_args
check_globals
uncompress "${DIR}/repository"
edit_sources

